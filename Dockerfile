FROM fullerb18/centos
#Thanks to: https://github.com/million12/docker-nginx MAINTAINER Marcin Ryzycki marcin@m12.io, Przemyslaw Ozgo linux@ozgo.info
MAINTAINER Ben Fuller bfuller@investability.com

# - Install basic packages (e.g. python-setuptools is required to have python's easy_install)
# - Install net-tools, small package with basic networking tools (e.g. netstat)
# - Install inotify, needed to automate daemon restarts after config file changes
# - Install yum-utils so we have yum-config-manager tool available
RUN \
  yum update -y && \
  yum install -y epel-release && \
  yum install -y net-tools python-setuptools hostname inotify-tools yum-utils && \
  yum clean all


# Add bootstrap.sh files
ADD Container-files /

VOLUME ["/data"]

ENTRYPOINT ["/config/bootstrap.sh"]
